use flatpak::prelude::*;

use std::{fs::File, io::BufReader, path::PathBuf};

use anyhow::Result;

use crate::models::{Backup, Component, Installation, Remote};

pub fn install(path: Option<PathBuf>) -> Result<()> {
    let path = path.unwrap_or_else(|| "flatpak-export.json".into());

    let file = File::open(path)?;
    let reader = BufReader::new(file);

    let backup: Backup = serde_json::from_reader(reader)?;

    for installation in backup.installations {
        let flatpak_installation = load_installation(&installation)?;

        for remote in installation.remotes {
            let remote = build_flatpak_remote(remote);
            flatpak_installation.add_remote(&remote, true, gio::Cancellable::NONE)?;
        }

        install_refs(&flatpak_installation, &installation.runtimes)?;
        install_refs(&flatpak_installation, &installation.apps)?;
    }

    Ok(())
}

fn build_flatpak_remote(remote: Remote) -> flatpak::Remote {
    let flatpak_remote = flatpak::Remote::new(&remote.name);

    flatpak_remote.set_url(remote.url.as_str());
    flatpak_remote.set_prio(remote.priority);

    flatpak_remote.set_default_branch(&remote.repo.default_branch.unwrap_or_default());
    flatpak_remote.set_title(&remote.repo.title.unwrap_or_default());
    flatpak_remote.set_comment(&remote.repo.comment.unwrap_or_default());
    flatpak_remote.set_description(&remote.repo.description.unwrap_or_default());
    flatpak_remote.set_icon(&remote.repo.icon.map(|u| u.to_string()).unwrap_or_default());
    flatpak_remote.set_homepage(
        &remote
            .repo
            .homepage
            .map(|u| u.to_string())
            .unwrap_or_default(),
    );
    flatpak_remote.set_collection_id(remote.repo.collection_id.as_deref());

    if let Some(key) = remote.repo.gpg_key {
        let bytes = glib::base64_decode(&key);
        flatpak_remote.set_gpg_key(&glib::Bytes::from(&bytes));
    }

    flatpak_remote
}

fn install_refs(installation: &flatpak::Installation, refs: &Vec<Component>) -> Result<()> {
    let transaction = flatpak::Transaction::for_installation(installation, gio::Cancellable::NONE)?;

    for component in refs {
        let ref_ = format!("{}//{}", component.id, component.branch);
        transaction.add_install(&component.origin, &ref_, &[])?;
    }

    transaction.run(gio::Cancellable::NONE)?;

    Ok(())
}

fn load_installation(installation: &Installation) -> Result<flatpak::Installation> {
    let installation = if installation.id == "user" {
        flatpak::Installation::new_user(gio::Cancellable::NONE)?
    } else if installation.id == "default" {
        flatpak::Installation::new_system(gio::Cancellable::NONE)?
    } else {
        anyhow::bail!("Custom installations are unsupported")
    };

    Ok(installation)
}
