use flatpak::prelude::*;

use anyhow::Context;
use serde::{Deserialize, Serialize};

/// The different types of component.
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub enum ComponentType {
    Runtime,
    App,
}

/// A component installed via flatpak.
///
/// Components can be apps, runtimes, SDKs,
/// or SDK extensions.
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub struct Component {
    /// The application ID
    pub id: String,
    /// The branch of the component installed, e.g. org.gnome.Sdk//42 vs. org.gnome.Sdk//master
    pub branch: String,
    /// The remote that the flatpak was installed from
    pub origin: String,
    // /// What kind of component this is
    // #[serde(rename = "type")]
    // pub type_: ComponentType,
}

impl TryFrom<flatpak::InstalledRef> for Component {
    type Error = anyhow::Error;

    fn try_from(value: flatpak::InstalledRef) -> Result<Self, Self::Error> {
        Ok(Self {
            id: value.name().context("Failed to get ref ID")?.to_string(),
            branch: value
                .branch()
                .context("Failed to get ref branch")?
                .to_string(),
            origin: value
                .origin()
                .context("Failed to get ref origin")?
                .to_string(),
        })
    }
}
