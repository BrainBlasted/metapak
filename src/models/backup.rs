use serde::{Deserialize, Serialize};

use super::Installation;

#[derive(Serialize, Deserialize, Debug)]
pub struct Backup {
    pub installations: Vec<Installation>,
}
