use flatpak::prelude::*;
use gio::prelude::*;

use anyhow::{Context, Result};
use serde::{Deserialize, Serialize};
use url::Url;

use std::path::PathBuf;

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub struct Remote {
    /// The name of the remote used by flatpak
    pub name: String,
    /// The url of the remote
    pub url: Url,
    /// The priority of the remote
    pub priority: i32,
    /// A representation of the flatpak repo
    #[serde(flatten)]
    pub repo: FlatpakRepo,
}

impl Remote {
    pub fn new(remote: &flatpak::Remote, installation: &flatpak::Installation) -> Result<Self> {
        let name = remote.name().context("Remote has no name")?.to_string();

        Ok(Self {
            name,
            url: remote
                .url()
                .and_then(|u| Url::parse(&u).ok())
                .context("context")?,
            priority: remote.prio(),
            repo: FlatpakRepo::new(remote, installation)?,
        })
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub struct FlatpakRepo {
    pub default_branch: Option<String>,
    pub title: Option<String>,
    pub comment: Option<String>,
    pub description: Option<String>,
    pub icon: Option<Url>,
    pub homepage: Option<Url>,
    pub collection_id: Option<String>,
    pub gpg_key: Option<String>,
}

impl FlatpakRepo {
    pub fn new(remote: &flatpak::Remote, installation: &flatpak::Installation) -> Result<Self> {
        let Some(remote_name) = remote.name().map(|n| n.to_string()) else {
            anyhow::bail!("Remote has no name");
        };

        let Some(installation_path) = installation.path().and_then(|p| p.peek_path()) else {
            anyhow::bail!("Installation has no path");
        };

        let gpg_key_path = installation_path
            .join("repo")
            .join(format!("{remote_name}.trustedkeys.gpg"));
        let gpg_key = load_gpg_key(gpg_key_path).ok();

        Ok(Self {
            default_branch: remote.default_branch().map(|f| f.to_string()),
            title: remote.title().map(|f| f.to_string()),
            comment: remote.comment().map(|f| f.to_string()),
            description: remote.description().map(|f| f.to_string()),
            icon: remote.icon().and_then(|f| Url::parse(&f).ok()),
            homepage: remote.homepage().and_then(|f| Url::parse(&f).ok()),
            collection_id: remote.collection_id().map(|f| f.to_string()),
            // TODO
            gpg_key,
        })
    }
}

fn load_gpg_key(path: PathBuf) -> Result<String> {
    let gpg_output = std::process::Command::new("sq")
        .args(["armor", path.to_str().unwrap()])
        .output()?
        .stdout;

    let key = String::from_utf8(gpg_output)?
        .trim()
        .lines()
        .filter(|l| !l.starts_with("-----"))
        .collect();

    Ok(key)
}
