use flatpak::prelude::*;
use gio::prelude::*;

use anyhow::Context;
use serde::{Deserialize, Serialize};

use std::path::PathBuf;

use super::{Component, Remote};

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
#[serde(rename_all = "lowercase")]
pub enum StorageType {
    Default,
    Network,
    Mmc,
    Sdcard,
    HardDisk,
}

impl From<flatpak::StorageType> for StorageType {
    fn from(value: flatpak::StorageType) -> Self {
        match value {
            flatpak::StorageType::Default => StorageType::Default,
            flatpak::StorageType::Network => StorageType::Network,
            flatpak::StorageType::Mmc => StorageType::Mmc,
            flatpak::StorageType::HardDisk => StorageType::HardDisk,
            flatpak::StorageType::Sdcard => StorageType::Sdcard,
            _ => unreachable!("{value}"),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub struct Installation {
    pub id: String,
    pub path: PathBuf,
    pub display_name: Option<String>,
    pub priority: i32,
    pub storage_type: StorageType,
    pub remotes: Vec<Remote>,
    pub runtimes: Vec<Component>,
    pub apps: Vec<Component>,
}

impl TryFrom<flatpak::Installation> for Installation {
    type Error = anyhow::Error;
    fn try_from(value: flatpak::Installation) -> Result<Self, Self::Error> {
        let remotes: Vec<Remote> = value
            .list_remotes(gio::Cancellable::NONE)?
            .iter()
            .filter_map(|r| Remote::new(r, &value).ok())
            .collect();
        let runtimes: Vec<Component> = value
            .list_installed_refs_by_kind(flatpak::RefKind::Runtime, gio::Cancellable::NONE)?
            .iter()
            .filter_map(|r| Component::try_from(r.clone()).ok())
            .filter(|c| {
                !c.id.ends_with(".Locale")
                    && !c.id.ends_with("GL.default")
                    && !c.id.ends_with("GL32.default")
            })
            .collect();
        let apps: Vec<Component> = value
            .list_installed_refs_by_kind(flatpak::RefKind::App, gio::Cancellable::NONE)?
            .iter()
            .filter_map(|r| Component::try_from(r.clone()).ok())
            .collect();

        Ok(Self {
            id: value.id().context("Installation has no name")?.to_string(),
            path: value
                .path()
                .and_then(|f| f.path())
                .context("Installation has no path")?,
            display_name: value.display_name().map(|g| g.to_string()),
            priority: value.priority(),
            storage_type: StorageType::from(value.storage_type()),
            remotes,
            runtimes,
            apps,
        })
    }
}
