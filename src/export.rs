use anyhow::Result;

use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;

use crate::models::{Backup, Installation};

pub fn export(path: Option<PathBuf>) -> Result<()> {
    let path = path.unwrap_or_else(|| "flatpak-export.json".into());

    let installations: Vec<Installation> = load_installations()?
        .iter()
        .filter_map(|i| Installation::try_from(i.clone()).ok())
        .collect();

    let backup = Backup { installations };

    let json = serde_json::to_string_pretty(&backup)?;
    let mut file = File::create(path)?;
    file.write_all(json.as_bytes())?;

    Ok(())
}

fn load_installations() -> Result<Vec<flatpak::Installation>> {
    let mut installations = flatpak::functions::system_installations(gio::Cancellable::NONE)?;
    installations.push(flatpak::Installation::new_user(gio::Cancellable::NONE)?);
    Ok(installations)
}
