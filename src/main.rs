use anyhow::Result;
use clap::{Parser, Subcommand};

use std::path::PathBuf;

mod export;
mod install;
mod models;

use export::export;
use install::install;

#[derive(Parser)]
#[command(
    about = "Export or import installed applications, repos, and runtimes to/from a json file."
)]
struct Cli {
    #[command(subcommand)]
    command: Command,
}

#[derive(Subcommand)]
pub enum Command {
    #[command(about = "Export installed repos, runtimes, and apps.")]
    Export {
        #[arg(help = "The path to export the file to.")]
        path: Option<PathBuf>,
    },
    #[command(about = "Install repos, runtimes, and apps from a json file.")]
    Install {
        #[arg(help = "The path to import the file from.")]
        path: Option<PathBuf>,
    },
}

fn main() -> Result<()> {
    let cli = Cli::parse();

    match &cli.command {
        Command::Export { path } => export(path.clone()),
        Command::Install { path } => install(path.clone()),
    }
}
